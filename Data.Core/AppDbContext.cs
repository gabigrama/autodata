﻿using AD.Core.Data;
using AD.Core.DomainModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Core
{
    public partial class AppDbContext : DbContext, IDbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
               : base(options)
        {
        }

        public virtual DbSet<Chasis> Chasis { get; set; }
        public virtual DbSet<ModelChasis> ModelChasis { get; set; }
        public virtual DbSet<Vehicle> Vehicle { get; set; }
        public virtual DbSet<VehicleAttribute> VehicleAttribute { get; set; }
        public virtual DbSet<VehicleModel> VehicleModel { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Chasis>(entity =>
            {
                entity.Ignore(e => e.Id);
                entity.Property(e => e.Id).HasColumnName("ChasisId");
                entity.Property(e => e.Name).HasMaxLength(50);

                entity.ToTable("Chasis");
            });

            modelBuilder.Entity<ModelChasis>(entity =>
            {
                entity.Ignore(e => e.Id);
                entity.HasKey(e => new { e.VehicleModelId, e.ChasisId });

                entity.ToTable("Model_Chasis");

                entity.HasOne(d => d.Chasis)
                    .WithMany(p => p.ModelChasises)
                    .HasForeignKey(d => d.ChasisId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Model_Chasis_Model_Chasis");

                entity.HasOne(d => d.VehicleModel)
                    .WithMany(p => p.ModelChasises)
                    .HasForeignKey(d => d.VehicleModelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Model_Chasis_Model");
            });

            modelBuilder.Entity<Vehicle>(entity =>
            {
                entity.Ignore(e => e.Id);
                entity.Property(e => e.Id).HasColumnName("VehicleId");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.ToTable("Vehicle");
            });

            modelBuilder.Entity<VehicleAttribute>(entity =>
            {
                entity.Ignore(e => e.Id);
                entity.Property(e => e.Id).HasColumnName("VehicleAttributeId");

                entity.Property(e => e.Color)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Chasis)
                    .WithMany(p => p.VehicleAttributes)
                    .HasForeignKey(d => d.ChasisId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VehicleAttribute_Chasis");

                entity.ToTable("VehicleAttribute");
            });

            modelBuilder.Entity<VehicleModel>(entity =>
            {
                entity.Ignore(e => e.Id);
                entity.Property(e => e.Id).HasColumnName("VehicleModelId");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.HasOne(d => d.Vehicle)
                    .WithMany(p => p.VehicleModels)
                    .HasForeignKey(d => d.VehicleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Model_Vehicle");

                entity.ToTable("VehicleModel");

            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

        #region Methods
        public virtual new DbSet<TEntity> Set<TEntity>() where TEntity:BaseEntity
        {
            return base.Set<TEntity>();
        }

        public override int SaveChanges()
        {
            return base.SaveChanges();
        }

        public IDbTransaction BeginTransaction(bool throwExOnRollback = true)
        {
            if(this.Database.CurrentTransaction == null)
            {
                this.Database.BeginTransaction();
            }
            return new AppDbTransaction(this, throwExOnRollback);
        }
        #endregion
    }
}
