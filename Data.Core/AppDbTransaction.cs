﻿using AD.Core.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Core
{
    public class AppDbTransaction : IDbTransaction
    {
        public IDbContext dbContext;
        private bool throwExOnRollback;

        public AppDbTransaction(IDbContext dbContext, bool throwExOnRollback)
        {
            if (dbContext == null)
                throw new ArgumentException(nameof(dbContext));

            this.dbContext = dbContext;
            this.throwExOnRollback = throwExOnRollback;
        }

        public void Commit()
        {
            (dbContext as DbContext).Database.CurrentTransaction.Commit();
        }

        public void Dispose()
        {
            Dispose();
        }

        public void Rollback()
        {
            (dbContext as DbContext).Database.CurrentTransaction.Rollback();
        }
    }
}
