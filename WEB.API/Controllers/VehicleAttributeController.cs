﻿using AD.Common.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.VehicleAttribute;
using Services.VehicleService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEB.API.Controllers
{
    [ApiController]
    public class VehicleAttributeController : ControllerBase
    {
        private readonly IVehicleAttributeService vehicleService;

        public VehicleAttributeController(IVehicleAttributeService vehicleService)
        {
            this.vehicleService = vehicleService;
        }

        [HttpGet]
        [Route("api/VehicleAttributes")]
        public void GetAttributes()
        {
            vehicleService.GetVehicleAttributes();

        }

        [HttpPost]
        [Route("api/VehicleAttributes")]
        public VehicleAttributeModel CreateVehicleAttribute(VehicleAttributeModel model)
        {

            return vehicleService.CreateVehicleAttribute(model);
        }
    }
}
