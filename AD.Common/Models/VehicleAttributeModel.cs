﻿namespace AD.Common.Models
{
    public class VehicleAttributeModel : BaseModel
    {
        public string Color { get; set; }
        public long ChasisId { get; set; }

        public virtual ChasisModel Chasis { get; set; }
    }
}