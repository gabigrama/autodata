﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AD.Common.Models
{
    public class VehicleModelModel : BaseModel
    {
        public VehicleModelModel()
        {
            Vehicles = new List<VehicleModel>();
        }

        public string Name { get; set; }
        public long VehicleId { get; set; }

        public List<VehicleModel> Vehicles{ get; set; }
    }
}
