﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AD.Common.Models
{
    public class ChasisModel : BaseModel
    {
        public ChasisModel()
        {
            VehicleAttributes = new List<VehicleAttributeModel>();
        }
        public string Name { get; set; }
        public List<VehicleAttributeModel> VehicleAttributes { get; set; }
    }
}
