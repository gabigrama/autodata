﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AD.Common.Models
{
    public class BlaBlaModel
    {
        public string Name { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
    }
}
