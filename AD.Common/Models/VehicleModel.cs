﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AD.Common.Models
{
    public class VehicleModel : BaseModel
    {
        public VehicleModel()
        {
            VehicleModels = new List<VehicleModelModel>();
        }
        public string Name { get; set; }
        public List<VehicleModelModel> VehicleModels { get; set; }
    }
}
