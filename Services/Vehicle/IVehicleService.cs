﻿using AD.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.VehicleService
{
    public interface IVehicleService
    {
        IEnumerable<VehicleModel> GetVehicles();
        VehicleModel GetVehicleById(long id);
    }
}
