﻿using System;
using System.Collections.Generic;
using AD.Core.DomainModel;
using System.Text;
using AD.Common.Models;

namespace Services.VehicleAttribute
{
    public interface IVehicleAttributeService
    {
        IEnumerable<AD.Core.DomainModel.VehicleAttribute> GetVehicleAttributes();
        VehicleAttributeModel CreateVehicleAttribute(VehicleAttributeModel model);
    }
}
