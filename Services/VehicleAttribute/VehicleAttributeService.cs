﻿using AD.Core.Data;
using System;
using System.Collections.Generic;
using System.Text;
using AD.Core.DomainModel;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using AD.Common.Models;
using AutoMapper;

namespace Services.VehicleAttribute
{
    public class VehicleAttributeService : IVehicleAttributeService
    {
        private readonly IMapper mapper;
        private readonly IRepository<AD.Core.DomainModel.VehicleAttribute> vehicleAttributeRepository;
        private readonly IRepository<AD.Core.DomainModel.Vehicle> vehicleRepository;
        private readonly IRepository<AD.Core.DomainModel.VehicleModel> vehicleModelRepository;
        private readonly IRepository<Chasis> chasisRepository;
        private readonly IRepository<ModelChasis> modelChasisRepository;
        public VehicleAttributeService(IRepository<AD.Core.DomainModel.VehicleAttribute> vehicleAttributeRepository,
            IRepository<AD.Core.DomainModel.Vehicle> vehicleRepository,
            IRepository<AD.Core.DomainModel.VehicleModel> vehicleModelRepository,
            IRepository<Chasis> chasisRepository,
            IRepository<ModelChasis> modelChasisRepository,
            IMapper mapper)
        {
            this.chasisRepository = chasisRepository;
            this.vehicleAttributeRepository = vehicleAttributeRepository;
            this.vehicleModelRepository = vehicleModelRepository;
            this.vehicleRepository = vehicleRepository;
            this.modelChasisRepository = modelChasisRepository;
            this.mapper = mapper;
        }
        public IEnumerable<AD.Core.DomainModel.VehicleAttribute> GetVehicleAttributes()
        {
            var query1 = vehicleAttributeRepository.Table
                         .Include(var => var.Chasis)
                         .ThenInclude(c => c.ModelChasises)
                         .ThenInclude(mc => mc.VehicleModel)
                         .ThenInclude(vm => vm.Vehicle);

            var query2 = from vehicleAttribute in vehicleAttributeRepository.TableNoTracking
                         join chasis in chasisRepository.TableNoTracking on vehicleAttribute.ChasisId equals chasis.Id
                         join modelChasis in modelChasisRepository.TableNoTracking on chasis.Id equals modelChasis.ChasisId
                         join model in vehicleModelRepository.TableNoTracking on modelChasis.VehicleModelId equals model.Id
                         join vehicle in vehicleRepository.TableNoTracking on model.VehicleId equals vehicle.Id
                         select new BlaBlaModel()
                         {
                             Name = vehicle.Name,
                             Model = model.Name,
                             Color = vehicleAttribute.Color
                         };

            return query1;
        }

        public VehicleAttributeModel CreateVehicleAttribute(VehicleAttributeModel model)
        {
            if (model == null)
                throw new NullReferenceException("Model is null");

            var entity = mapper.Map<AD.Core.DomainModel.VehicleAttribute>(model);

            using IDbTransaction transaction = vehicleAttributeRepository.CreateTransaction();

            try
            {
                vehicleAttributeRepository.Insert(entity);
                transaction.Commit();
            }
            catch(Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }

            vehicleAttributeRepository.DbContext.SaveChanges();


            return mapper.Map<VehicleAttributeModel>(entity);
        }
    }
}
