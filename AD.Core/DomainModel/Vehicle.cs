﻿using AD.Core.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace AD.Core.DomainModel
{
    public class Vehicle : BaseEntity
    {
        public Vehicle()
        {
            VehicleModels = new HashSet<VehicleModel>();
        }

        public string Name { get; set; }

        public virtual ICollection<VehicleModel> VehicleModels { get; set; }
    }
}
