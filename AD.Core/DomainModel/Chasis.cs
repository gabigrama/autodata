﻿using AD.Core.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace AD.Core.DomainModel
{
    public class Chasis : BaseEntity
    {
        public Chasis()
        {
            ModelChasises = new HashSet<ModelChasis>();
            VehicleAttributes = new HashSet<VehicleAttribute>();
        }

        public string Name { get; set; }

        public virtual ICollection<ModelChasis> ModelChasises { get; set; }
        public virtual ICollection<VehicleAttribute> VehicleAttributes { get; set; }

    }
}
