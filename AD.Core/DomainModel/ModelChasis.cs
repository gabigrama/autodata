﻿using AD.Core.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace AD.Core.DomainModel
{
    public class ModelChasis : BaseEntity
    {
        public long VehicleModelId { get; set; }
        public long ChasisId { get; set; }

        public virtual Chasis Chasis { get; set; }
        public virtual VehicleModel VehicleModel { get; set; }
    }
}
