﻿using AD.Core.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace AD.Core.DomainModel
{
    public class VehicleAttribute : BaseEntity
    {
        public VehicleAttribute()
        {

        }
        public string Color { get; set; }
        public long ChasisId { get; set; }

        public virtual Chasis Chasis { get; set; }
    }
}
