﻿using AD.Core.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace AD.Core.DomainModel
{
    public class VehicleModel : BaseEntity
    {
        public VehicleModel()
        {
            ModelChasises = new HashSet<ModelChasis>();
        }

        public long VehicleId { get; set; }
        public string Name { get; set; }

        public virtual Vehicle Vehicle { get; set; }
        public virtual ICollection<ModelChasis> ModelChasises { get; set; }
    }
}
