﻿using AD.Common.Models;
using AD.Core.DomainModel;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace AD.Core.Automapper
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<AD.Common.Models.VehicleModel, Vehicle>().ForMember(s => s.VehicleModels, d=> d.Ignore());
            CreateMap<Vehicle, AD.Common.Models.VehicleModel>();

            CreateMap<VehicleModelModel, AD.Core.DomainModel.VehicleModel>().ForMember(s => s.ModelChasises, d => d.Ignore())
                                                                            .ForMember(s => s.Vehicle,d => d.Ignore());
            CreateMap<AD.Core.DomainModel.VehicleModel, VehicleModelModel>();

            CreateMap<ChasisModel, Chasis>().ForMember(s => s.ModelChasises, d=> d.Ignore())
                                            .ForMember(s => s.VehicleAttributes, d=>d.Ignore());
            CreateMap<Chasis, ChasisModel>();

            CreateMap<VehicleAttribute, VehicleAttributeModel>();
            CreateMap<VehicleAttributeModel, VehicleAttribute>().ForMember(s => s.Chasis, d => d.Ignore());
        }
    }
}
