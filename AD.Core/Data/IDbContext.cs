﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AD.Core.Data
{
    public interface IDbContext
    { 
        DbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity;
        int SaveChanges();
        IDbTransaction BeginTransaction(bool throwExOnRollback);
    }
}
