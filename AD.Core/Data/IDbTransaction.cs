﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AD.Core.Data
{
    public interface IDbTransaction : IDisposable
    {
        void Commit();
        void Rollback();
    }
}
