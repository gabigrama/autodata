﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AD.Core.Data
{
    public abstract class BaseEntity
    {
        public long Id { get; set; }
    }
}
